package ch.shinungo.pejo;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.shinungo.pejo.model.Access;
import ch.shinungo.pejo.model.Account;
import ch.shinungo.pejo.model.Balance;
import ch.shinungo.pejo.model.ConsentRequest;
import ch.shinungo.pejo.model.ConsentResponse;
import ch.shinungo.pejo.model.Transaction;
import ch.shinungo.pejo.repository.Quote;
import ch.shinungo.pejo.repository.User;
import ch.shinungo.pejo.repository.UserRepository;

@SpringBootApplication
public class PejoMainApplication {	
		private static final String CONSENT_URL = "https://api.dev.openbankingproject.ch/v1/consents";
		private static final Logger log = LoggerFactory.getLogger(PejoMainApplication.class);

	// STARTMETHODE: 
	public static void main(String[] args) {
		SpringApplication.run(PejoMainApplication.class, args);
	}
	
	@Bean
	public void getContet() throws JsonProcessingException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Request-ID", "99391c7e-ad88-49ec-a2ad-99ddcb1f7721"); 
		headers.set("PSU-ID" , "buha1");                 // 
		headers.set("psu-ip-address" , "192.168.0.2");  // IPv4: 192.168.1.5 // Standardgateway 192.168.1.1  
		headers.set("tpp-redirect-uri" , "url-class-java-examples");
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON); 
		
				
		ObjectMapper mapper = new ObjectMapper();
	
		//Object to JSON in String
		String jsonInString = mapper.writeValueAsString(getConsentRequest());
		log.debug("JETZT KOMMT DER JSON-STRING");
		log.debug(jsonInString);
		
		/*
		 * Bildung einer HttpEntität + eines RestTemplates. 
		 * Beide werden "verheiratet"als entityReq
		 *  an die ConsentURL als POST geschickt. 
		 */
		
		HttpEntity<String> entityReq = new HttpEntity<String>(jsonInString, headers);
		RestTemplate template = new RestTemplate();
		ResponseEntity<ConsentResponse> respEntity = template   // HIer Zugriff angefragt. 
		    .exchange(CONSENT_URL, HttpMethod.POST, entityReq, ConsentResponse.class);
		log.debug("JETZT KOMMT DER BODY MIT DER CONSENT-ID");
		log.debug(respEntity.getBody().getConsentId());	

		/*
		 * Folgende Methode muss erstellt werden: 
		 */
		
		/*  22.06.2020:  
		 * 
		 * BUTTON im userChooser.html WURDE GEDRÜCKT
		 * 
		 * 0. getSelectet the selceted IBAN-Kto.  
		 * 1. read respEntity.
		 * 2. Hier die ConsentId in eine Variable speichern  // "private static final"
		 * 3. save Link (==>  body/Links/scaRedirect/href) in Variable "sessionConfirmerURL" 
		 * 4. Anzeigen des ContentIdConfirmer.html  
		 *     
		 * 5. Wenn USER Erfolgreich in Bank verifiziert, und auf unsere psu-ip-address zurückommt: 

		 *  
		 *  public void makeAuthentificationComplete() {
		 *  	1. Authentification-Dings vom im 5 erhaltenen POST der BANK empfangen. 
		 *  	2. Diese in einem neuen "template" mit Authentificaton wie mit "getContet" an die Bank schicken
		 *  	3. Anzeige von Template "showAccount" (IBAN; USER, --Kontostand???)  
		 *  }
		 * 
		 */
		
		
	}

	private ConsentRequest getConsentRequest() {
	
		ConsentRequest cr = new ConsentRequest();
		cr.setAccess(new Access());
		cr.setRecurringIndicator(false);
		cr.setValidUntil("2020-12-31");
		cr.setFrequencyPerDay(4);
		cr.setCombinedServiceIndicator(false);
		
		Account a = new Account();
		a.setIban("CH2810051000000000054");
		cr.getAccess().setAccounts(new ArrayList<Account>());
		cr.getAccess().getAccounts().add(a);
		
		Balance b = new Balance(); 
		b.setIban ("CH2810051000000000054");
		cr.getAccess().setBalances(new ArrayList<Balance>());
		cr.getAccess().getBalances().add(b);
		
		Transaction tra = new Transaction();
		tra.setIban("CH2810051000000000054");
		cr.getAccess().setTransactions(new ArrayList<Transaction>());
		cr.getAccess().getTransactions().add(tra);
		return cr;
	}	
}