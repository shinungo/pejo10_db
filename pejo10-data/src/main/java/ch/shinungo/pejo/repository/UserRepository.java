package ch.shinungo.pejo.repository;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	// public User findByUser(String name);

}
